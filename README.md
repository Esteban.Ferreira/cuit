Que es el DNI

(Según el Registro Nacional de Personas - RENAPER...)
El DNI es el Documento Nacional de Identidad que certifica en forma exclusiva la identidad de las personas, a partir de la base de datos que administra el Registro Nacional de las Personas. El DNI es el único instrumento que acredita la identidad de una persona.
Explícitamente en ningún lado indica que es único, pero todos creemos que así lo es. 
Luego de haber trabajado con bases de datos de la Argentina, me di cuenta que esto no es tan así.  
Encontré una gran cantidad de personas que tienen su DNI duplicado. Es decir que hay dos personas en la Argentina con el mismo número.
Puede ser un poco complicado de entender al principio, pero tuve la suerte y/o la coincidencia de tener un compañero cercano con este "problema". Su número de DNI era el mismo que de una chica. Gracias a él empecé a investigar y descubrí que es algo común (si es que se puede llamar así).
Aparentemente el ReNaPer debido a algún error entrego números duplicados.  
La explicación más lógica: la distribución de los números depende de personas.. Y donde existe una persona... existe la posibilidad de equivocación...  
Es por ello que (entre otros motivos)... para facilitar el trabajo de organismos oficiales (ANSES, AFIP), surgió el identificador del CUIL(Código Único de Identifación Laboral) - CUIT (Código único de Identificación Tributaría). 
De esta manera, por más que hubiese un DNI duplicado el cuil los diferenciaría, y no habría más problemas...
Para ello se inventó un Algoritmo para calcular dicho número (CUIL-CUIT - Son los mismos números).

CUIL / CUIT	

CUIL/T: Son 11 números en total:  
XY - 12345678 - Z
XY: Indican el tipo (Masculino, Femenino o una empresa)
12345678: Número de DNI
Z: Código Verificador
Algoritmo:
Se determina XY con las siguientes reglas 
Masculino:20
Femenio:27
Empresa:30
Se multiplica XY 12345678 por un número de forma separada:
X * 5
Y * 4
1 * 3
2 * 2
3 * 7
4 * 6
5 * 5
6 * 4
7 * 3
8 * 2
Se suman dichos resultados. El resultado obtenido se divide por 11. De esa división se obtiene un Resto, que determina Z 
Si el resto es 0= Entonces Z=0 Si el resto es 1= Entonces se aplica la siguiente regla:
*Si es hombre: Z=9 y XY pasa a ser 23 *Si es mujer: Z=4 y XY pasa a ser 23
Caso contrario XY pasa a ser (11- Resto).

Ejemplo:   
Masculino DNI 12 345 678 

1-Determinar el Tipo 
XY es 20
Hacemos el cálculo 
2 * 5=10
0 * 4=0
1 * 3=3
2 * 2=4
3 * 7=21
4 * 6=24
5 * 5=25
6 * 4=24
7 * 3=21
8 * 2=16
Realizamos la suma de (10+0+3+4+21+24+25+24+21+16)= 148  
Dividimos por 11 para obtener Z (el código verificador)
148/11=13,4545--->13 (Redondeo)
Obtenemos el resto de la división 
148 - (13*11)=5
Determinamos Z 
11-5= 6

Conclusión: CUIL-CUIT
20-12345678-6

Básicamente esta es la regla por la cual se debería determinar el número de CUIL/T....
Y repito debería pues si bien lo genera este algoritmo, el impulsor del mismo es una persona.
Es por ello que existen personas con número de CUIL que no se ajustan a esta reglas (o si en cierta manera).

Los errores más comunes son:
Por ejemplo a: 12345678
Se equivocan de sexo ( En vez de masculino le colocan F a la hora del cálculo)
Resultado: 27 - 12345678 - 0
Se equivocaron de número en el DNI
Resultado: 20 - 12346788 - 5
